const marked = require('marked')

export default function (options) {
  return {
    name: 'vitePluginVueMarkdown',
    transform(src, id) {
      let isInclude = true
      if (options && Array.isArray(options.dirs)) {
        isInclude = options.dirs.some(item => id.indexOf(item) != -1)
      }
      if (isInclude && id.endsWith(".md")) {
        return {
          code: `import {h, defineComponent} from "vue";
                import '@/assets/markdown.less'
                import Prism from 'prismjs';
                import '@/assets/prism.css';

                const _sfc_md = defineComponent({
                    name: "Markdown",
                    async mounted() {
                      await this.$nextTick();
                      Prism.highlightAll();
                    }
                });

                const _sfc_render =() => {
                    return h("div", {
                      innerHTML: '<div class="markdown-body">${JSON.stringify(marked(src)).replace(/^(\s|")+|(\s|")+$/g, '')}</div>', 
                    })
                };

                _sfc_md.render = _sfc_render
                export default _sfc_md`,
          map: null
        }
      }
    }
  }
}