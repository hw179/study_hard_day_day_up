const fs = require('fs');
const path = require('path');

export function getVueFilesInDir(dirPath) {
  const vueFiles = [];

  // 读取目录下的所有文件
  const files = fs.readdirSync(dirPath);
  // 遍历所有文件，判断是文件还是文件夹
  files.forEach(file => {
    const fullPath = `${dirPath}/${file}`//path.join(dirPath, file);
    const stats = fs.statSync(fullPath);

    // 如果是文件夹，则递归获取该文件夹下的所有Vue文件
    if (stats.isDirectory()) {
      vueFiles.push(...getVueFilesInDir(fullPath));
    } else if (path.extname(file) === '.vue') {
      // 如果是Vue文件，则将文件路径添加到数组中
      vueFiles.push(fullPath);
    }
  });

  return vueFiles;
}