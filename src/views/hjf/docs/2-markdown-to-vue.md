# markdown转成vue组件优化

## 问题

此前，markdown转vue组件的做法，一个md文件对应一个vue组件，明显不合理，解决方案有以下三个：

### 方案1

```HTML
<!-- /views/hjf/docs/index.vue -->
<template>
  <div id="mdPreviewContainer" class="markdown-body"></div>
</template>
<script>
import '@/assets/markdown.less'
import Prism from 'prismjs';
import '@/assets/prism.css';

import { createApp, defineComponent } from 'vue';

export default defineComponent({
  name: 'DocPreview', 
  props: {
    name: String
  },
  async mounted() {
    await this.registerComponent(this.name, 'mdPreviewContainer')
    await this.$nextTick();
    Prism.highlightAll();
  },
  methods:{
    registerComponent(templateName,id) {
      return import('./' + templateName + '.md').then(component => {
        const app = createApp(component.default)
        app.mount('#' + id);
      });
    }
  }
})
</script>
```

### 方案2

```HTML
<!-- /views/hjf/docs/index.vue -->
<template>
  <div class="markdown-body">
    <component :is="DocComp"/>
  </div>
</template>
<script setup>
  import '@/assets/markdown.less'
  import Prism from 'prismjs';
  import '@/assets/prism.css';
  import { shallowRef, watch, onMounted, onUpdated, nextTick } from 'vue';
  import { useRoute } from "vue-router"

  const route = useRoute();
  const DocComp = shallowRef();
  
  watch(
    () => route.params.name,
    async name => {
      try {
        DocComp.value = (await import(`./${name}.md`)).default
      } catch {
        // router.push('/404')
      }
    },
    { immediate: true }
  )

  onUpdated(async() => {
    await nextTick();
    Prism.highlightAll();
  })
</script>
```

以上两个方案，路由配置如下：
```Javascript
···
{
  path: '/hjf',
  name: 'hjf',
  component: () => import('@/views/hjf/index.vue'),
  children: [{
    path: 'docs/index/:name',
    name: 'docIndex',
    component: () => import('@/views/hjf/docs/index.vue'),
    props: true
  }]
}
···
```

### 方案3

直接修改转换插件 vite-plugin-vue-markdown.js

```JavaScript
// plugins/vite-plugin-vue-markdown.js

const marked = require('marked')

export default function (options) {
  return {
    name: 'vitePluginVueMarkdown',
    transform(src, id) {
      let isInclude = true
      if (options && Array.isArray(options.dirs)){
        isInclude = options.dirs.some(item => id.indexOf(item) != -1)
      }
      if (isInclude && id.endsWith(".md")) {
        return {
          code: `import {h, defineComponent} from "vue";
                import '@/assets/markdown.less'
                import Prism from 'prismjs';
                import '@/assets/prism.css';

                const _sfc_md = defineComponent({
                    name: "Markdown",
                    async mounted() {
                      await this.$nextTick();
                      Prism.highlightAll();
                    }
                });

                const _sfc_render =() => {
                    return h("div", {
                      innerHTML: '<div class="markdown-body">${JSON.stringify(marked(src))}</div>', 
                    })
                };

                _sfc_md.render = _sfc_render
                export default _sfc_md`,
          map: null
        }
      }
    }
  }
}
```

路由配置如下，直接引入md文件
```JavaScript
{
  path: '/hjf',
  name: 'hjf',
  component: () => import('@/views/hjf/index.vue'),
  children: [{
    path: 'docs/markdown-to-vue',
    name: 'markdownToVue',
    component: () => import('@/views/hjf/docs/1-markdown-to-vue.md'),     
    props: true
  }]
}
```