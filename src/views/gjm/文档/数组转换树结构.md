# 数组转换为树结构


## 使用递归转换
```javascript

  function getParentData(data = [], item) {
    let pData = null
    for (let el of data) {
      if (el.id === item.pid) {
        pData = el;
        break;
      } else if (Array.isArray(el.children) && el.children.length > 0) {
        pData = getParentData(el.children, item)
        if (pData) break;
      }
    }
    return pData;
  }

  function arrayToTreeByRecursion(data = []) {
    let result = [];
    data.forEach((item) => {
      let pData = getParentData(result, item)
      if (pData) {
        pData.children.push({ ...item, children: [] })
      } else {
        result.push({ ...item, children: [] })
      }
    })
    return result;
  }
```

## 不适用递归实现

  主要思路也是先把数据转成Map去存储，之后遍历的同时借助对象的引用，直接从Map找对应的数据做存储。不同点在遍历的时候即做Map存储,有找对应关系。性能会更好。
  ```javascript 
  function arrayToTreeByMap(items) {
    let result = [];
    let itemsMap = {};
    items.forEach(item => {
      let { id, pid } = item;
      if (!itemsMap[id]) {
        itemsMap[id] = {
          children: []
        }
      }

      itemsMap[id] = {
        ...item,
        children: itemsMap[id].children
      }
      let treeItem = itemsMap[id];

      
      if (pid === null) {
        result.push(treeItem);
      } else {
        if (!itemsMap[pid]) {
          itemsMap[pid] = {
            children: []
          }
        } 
        itemsMap[pid].children.push(treeItem)
      }
    })
    return result;
  }
  ```

  > 数据结构如下
```javascript
[
    {
        "id": 1,
        "pid": null
    },
    {
        "id": 2,
        "pid": 1
    },
    {
        "id": 3,
        "pid": 1
    },
    {
        "id": 4,
        "pid": 3
    },
    {
        "id": 5,
        "pid": 1
    }
]
```
> 转换后数据如下
```javascript
{
    "id": 1,
    "pid": null,
    "children": [
        {
            "id": 2,
            "pid": 1,
            "children": []
        },
        {
            "id": 3,
            "pid": 1,
            "children": [
                {
                    "id": 4,
                    "pid": 3,
                    "children": []
                }
            ]
        },
        {
            "id": 5,
            "pid": 1,
            "children": []
        }
    ]
}
```

  ># 总结

   使用递归的方式，当数据量过大的时候，耗时太久，不使用递归的方式性能高很多吗，具体可以在demo中查看耗时

