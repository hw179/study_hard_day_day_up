# flex自动撑满高度不生效问题

- demo例子路径  src\views\gjm\layout\flex1.vue

## 一、flex自动撑满高度
一个简单的自动撑满高度布局，只需要创建一个flexbox容器，里面包含box1 box2,box3 三个item项，box1,box3固定高度，box2 设置为flex: 1，中间的容器自动撑满高度,代码如下

**以下的例子不要嵌套在一个flexbox布局里面**
```html
<div class="flex-test-box">
    <div class="header"></div>
    <div class="main">
      <ul>
        <!-- li要有多个，足以撑满整个容器，这里为了排版省略 -->
        <li>this is text</li>
      </ul>
    </div>
    <div class="footer"></div>
  </div>
```

```less
.flex-test-box {
  display: flex;
  height: 100%;
  flex-direction: column;
  .header {
    height: 50px;
    background-color: rgb(62, 201, 12);
  }
  .main {
    flex: 1;
    background-color: #1c74e7;
    overflow-y: auto; 
  }
  .footer {
    height: 50px;
    background-color: #d12121;
  }
}
```

## 二、多层flex自动撑满高度
改造一下dom结构，如下
```html
  <div class="flex-test-box">
    <div class="header"></div>
    <div class="main">
      <div class="tab-box"></div>
      <div class="main-content">
        <ul>
          <!-- li要有多个，足以撑满整个容器，这里为了排版省略 -->
          <li>this is text</li>
        </ul>
      </div>
    </div>
    <div class="footer"></div>
  </div>
```
```less
.flex-test-box {
  display: flex;
  height: 100vh;
  flex-direction: column;
  .header {
    height: 50px;
    background-color: rgb(62, 201, 12);
  }
  .main {
    flex: 1;
    background-color: #1c74e7;
    display: flex;
    flex-direction: column;
    .tab-box {
      height: 50px;
      background-color: #6e5454;
    }
    .main-content {
      flex: 1;
      background: #d16ecc;
      overflow-y: auto;
    }
  }
  .footer {
    height: 50px;
    background-color: #d12121;
  }
  ```

  按照上诉的代码，自动撑满高度失效了

  **解决问题只需要给.main 容器中设置 height: 0 即可**

  ## [flex语法链接](https://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)

# **原理**
- 默认情况下，flex选项不会缩小低于他的最小内容尺寸
- 概括就是：默认最小尺寸是内容尺寸，所以导致溢出，直接设置为0就可以让最小尺寸低于内容尺寸来解决溢出问题
- flex-grow 用于控制项目在主轴方向上拉伸放大占剩余空间（如果有的话）的比，默认值：0，不放大。
- flex-shrink 用于控制项目在主轴方向上缩小的程度。数值越大，收缩越多,默认是 flex-shrink: 1 ，同等比例收缩。
- flex-basis 用于初始化每个项目占据主轴空间的尺寸,默认值：flex-basis: auto , 自动检测尺寸，如果项目有设置尺寸，则取项目设置的值，否则根据项目内容计算出尺寸。
- flex 是上面三个合并的简写, flex: <flex-grow> [<flex-shrink>] [<flex-basis>];默认值：flex: 0 1 auto; 后两个参数为可选参数。

