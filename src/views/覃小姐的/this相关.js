/*
 * @Author: 951452292 951452292@qq.com
 * @Date: 2023-04-10 09:03:50
 * @LastEditors: 951452292 951452292@qq.com
 * @LastEditTime: 2023-04-10 11:05:48
 * @FilePath: /study_hard_day_day_up/src/views/覃小姐的/this相关.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
var name = "the window";
var object = {
    name: 'my object',
    getValue: () => console.log(this.name),
    getNameFun: function () {
        return function () {
            return this.name;
        }
    },
    say: function () {
        setTimeout(function(){
            console.log(this)
        })
    }
}

console.log(object.getValue())
console.log(object.getNameFun()());
console.log(object.say())


// 1.独立函数，闭包函数，定时器函数里面的this指向window。
// 2.对象里面的字段函数使用箭头函数，其this指向window，对象没有作用域，按照箭头函数的this指向，是找最近function作用域中的this。
// 3.普通函数的this是指向调用者，箭头函数的this就是其定义的时的代码结构决定。



