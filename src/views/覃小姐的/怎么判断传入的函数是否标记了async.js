/*
 * @Author: 951452292 951452292@qq.com
 * @Date: 2023-03-08 09:25:15  Qin000333000
 * @LastEditors: 951452292 951452292@qq.com
 * @LastEditTime: 2023-03-09 13:58:12
 * @FilePath: /study_hard_day_day_up/src/views/覃小姐的/怎么判断传入的函数是否标记了async.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/** 面试题
  * 判断传入的函数是否标记了async
*/
function isAsyncFunction (func) {}

isAsyncFunction(() => {}) // expect:false
isAsyncFunction(async () => {}) // expect:true

/** 解析：
* 1.直接在方法里面调用func函数，那这个有副作用：
    * 1）如果方法里面有ajax请求呢，
    * 2）或者该函数参数你不确定入参，
    * 3）函数本事没async，但是它返回的是一个promise，烨判断不出来的； 
    * 4）或者该函数修改全局参数等，故调用是肯定不行的。
* 2.普通函数和标记了async函数到底有什么区别
  * async Symbol[Symbol.toStringTag] : 'AsyncFunction'
*/

  // 所以只要判断这个函数带不带这个知名符号就可以了

  function isAsyncFunction (func) {
    return func[Symbol.toString] === 'AsyncFunction'
  }
/**
*    isAsyncFunction(() => Promise.resolve()) // false,就可以精确的判断传入的函数是否是标记了async
*  3.Symbol.toString是什么？
*    我们判断一个字段是什么类型：Object.prototype.toString.call([]) // '[Object Array]'
*    返回的就是那个知名符号

*   故函数我们可以修改为：
*/

  function isAsyncFunction (func) {
    return Object.prototype.toString.call(func) ==='[Object AsyncFunction]'
  }

  // 就可以了。