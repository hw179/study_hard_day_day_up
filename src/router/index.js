import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/hua',
      name: 'hua',
      component: () => import('../views/华超伟/index.vue')
    },
    {
      path: '/gjm',
      name: 'gjm',
      component: () => import('@/views/gjm/index.vue'),
      children: [{
        path: 'layout/flex1',
        name: 'flex1',
        component: () => import('@/views/gjm/layout/flex1.vue')
      }, {
        path: 'data/tree',
        name: 'tree',
        component: () => import('@/views/gjm/tree/index.vue'),
        children: [{
          path: 'doc',
          name: 'treeDoc',
          component: () => import('@/views/gjm/文档/数组转换树结构.md')
        }]
      }, {
        path: 'other/draggable',
        name: 'draggable',
        component: () => import('@/views/gjm/draggable/drag.vue')
      },]
    },
    {
      path: '/hjf',
      name: 'hjf',
      component: () => import('@/views/hjf/index.vue'),
      children: [{
        path: 'docs/markdown-to-vue',
        name: 'markdownToVue',
        component: () => import('@/views/hjf/docs/1-markdown-to-vue.md'),
        props: true
      }, {
        path: 'docs/markdown-to-vue-optimize',
        name: 'markdownToVueOptimize',
        component: () => import('@/views/hjf/docs/2-markdown-to-vue.md'),
        props: true
      }, {
        path: 'style/backdrop-filter',
        name: 'backdrop-filter',
        component: () => import('@/views/hjf/style/backdrop-filter.vue')
      }]
    }
  ]
})

export default router
