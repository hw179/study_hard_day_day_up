import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import path from 'path'
import vitePluginVueMarkdown from "./plugins/vite-plugin-vue-markdown";
import { getVueFilesInDir } from "./utils/nodeUtils"

// console.log(getVueFilesInDir('./src/views/gjm'))
const groupJm = getVueFilesInDir('./src/views/gjm');

const pathSrc = path.resolve(__dirname, 'src')

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vitePluginVueMarkdown({
      dirs: [
        'views/hjf/docs',
        'views/gjm/文档',
      ]
    }),
    vueJsx(),
    AutoImport({
      resolvers: [
        // ElementPlusResolver(),
        // 自动导入图标组件
        IconsResolver({
          prefix: 'Icon',
        }),
      ],
    }),
    Components({
      resolvers: [
        // ElementPlusResolver(),
        // 自动注册图标组件
        IconsResolver({
          enabledCollections: ['ep'],// 重点     <el-icon :size="20"><i-ep-Document /></el-icon> 一这种格式编写
        }),
      ],
      dts: path.resolve(pathSrc, 'auto-imports.d.ts'),
    }),
    Icons({
      autoInstall: true,
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  //本地运行配置，以及反向代理配置
  server: {
    host: "0.0.0.0",
    https: false,//是否启用 http 2
    cors: true,//为开发服务器配置 CORS , 默认启用并允许任何源
    open: true,//服务启动时自动在浏览器中打开应用
    port: "5173",
    strictPort: false, //设为true时端口被占用则直接退出，不会尝试下一个可用端口
    force: true,//是否强制依赖预构建
    hmr: false,//禁用或配置 HMR 连接
    // 反向代理配置
    proxy: {
      //  '/api': {
      //   target: "https://xxxx.com/",
      //   changeOrigin: true,
      //   rewrite: (path) => path.replace(/^/api/, '')
      //  }
    },
    rollupOptions: {
      // https://rollupjs.org/guide/en/#outputmanualchunks
      output: {
        manualChunks: {
          'group-jm': groupJm,
        },
      },
    },
  },
  build: {
    rollupOptions: {
      // https://rollupjs.org/guide/en/#outputmanualchunks
      output: {
        manualChunks: {
          'group-jm': groupJm,
        },
      },
    },
  },
})
